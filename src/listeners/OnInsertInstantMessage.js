import {DataError} from '@themost/common';
import { DataObjectState } from '@themost/data';
// eslint-disable-next-line no-unused-vars
async function beforeSaveAsync(event) {
    //
} 

async function afterSaveAsync(event) {
    //
    if (event.state === DataObjectState.Insert) {
        // forward message to service
    }
} 

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}